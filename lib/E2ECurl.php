<?php

class E2ECurl
{
	/**
     * @var string
     */
	protected $host = '';
	
	/**
     * @var string
     */
	protected $api_key = '';
	
	/**
     * @var string
     */
	protected $company_key = '';
	
	/**
     * @var string
     */
	protected $content_type = '';
	
	public function __construct($host, $api_key, $company_key)
    {
        $this->host = $host;
        $this->api_key = $api_key;
        $this->company_key = $company_key;
    }
    
    /**
     * @param string $method
     * @param string $path
     * @param string $class
     * @param array $params
     * @param string $body
     * @return object|array|null an single or an array of $class instances
     */
    public function request($method, $path, $class, $params = array(), $body = null)
    {
		$this->content_type = 'application/json';
		$response = $this->request2($method, $path, $class, $params, $body);
		return $response;
	}
	    
    /**
     * @param string $method
     * @param string $path
     * @param string $class
     * @param array $params
     * @param string $body
     * @return object|array|null an single or an array of $class instances
     */
    public function query($method, $path, $class, $params = array(), $body = null)
    {
		$this->content_type = 'application/x-www-form-urlencoded';
		$response = $this->request2($method, $path, $class, $params, $body);
		return $response;
	}
	
    /**
     * @param string $method
     * @param string $path
     * @param string $class
     * @param array $params
     * @param string $body
     * @return object|array|null an single or an array of $class instances
     */
    private function request2($method, $path, $class, $params = array(), $body = null)
    {
        // Initiate curl
        $ch = curl_init();
        
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        // Set the url
        $url = self::requestUrl($path, $params);
        curl_setopt($ch, CURLOPT_URL, $url);
        
        // Set method
        curl_setopt($ch, CURLOPT_POST, $method == 'POST');
        if (!empty($body)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }
        
        // Headers
        $headers = array();
        $headers []= 'Accept: application/json';
        if (!empty($body)) {
            $headers []= 'Content-Type: '.$this->content_type;
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        // Execute
        $result=curl_exec($ch);
        
        // Response Status code
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        // Closing
        curl_close($ch);
        
        // Error?
        if ($httpcode < 200 || $httpcode > 299) {
            throw new \Swagger\Client\ApiException("", $httpcode, array(), $result);
        }
        
        // Unmarshal Response
        $obj = null;
        if (!empty($result) && $httpcode != 204) {
            $content = json_decode($result);
            if (empty($class)) {
                $obj = $content;
            } else {
                $obj = \Swagger\Client\ObjectSerializer::deserialize($content, $class);
            }
        }
        
        return $obj;
    }
    
    /**
     * @param string $path
     * @param array $params
     * @return string
     */
    protected function requestUrl($path, $params)
    {
        if (empty($params)) {
            $params = array();
        }
        
        $params['api_key'] = $this->api_key;
        $params['company_key'] = $this->company_key;
        
        $query = array();
        foreach ($params as $k => $v) {
            if (!is_array($v)) {
                $query []= $k.'='.urlencode($v);
            } else {
                foreach ($v as $v2) {
                    $query []= $k.'='.urlencode($v2);
                }
            }
        }
        
        $url = $this->host.'/'.$path;
        
        if (!empty($query)) {
            $url .= '/?'.implode('&', $query);
        }
        
        return $url;
    }
}
