# Exemplos
Exemplos de invocação da API E2E.pt utilizando o modelo de dados e cliente CURL.

## Account

### GET

```php
$class = '\Swagger\Client\Model\AccountListContainer';
$params = array('vatNumber'=>'123456789');
$result = self::request('GET', 'account', $class, $params);
```

### POST

```php
$class = '\Swagger\Client\Model\Account';
$body = $account->__toString();
$account = self::request('POST', 'account', $class, null, $body);
```
